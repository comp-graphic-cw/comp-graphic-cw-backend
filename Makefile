update-python-deps:
	wget -O /tmp/fractal-generator.zip https://github.com/akorunska/fractal-generator/archive/master.zip
	rm -rf fractal-generator-master | true
	unzip /tmp/fractal-generator
	rm -f /tmp/fractal-generator.zip
	cd fractal-generator-master; python3 -m venv env/ \
	source env/bin/activate; \
	pip3 install -r requirements.txt; \
	python3 fractal_generator.py -f julia  -a 500 -b 500 -x 0 -y 0 -i 250 -j 350 -z 12 -p 3 -c \#ffffff -o my_julia.png

init-devspace:
	cp .env.example .env
	mkdir public

gen:
	cd fractal-generator-master; \
	env/bin/python fractal_generator.py -f mandelbrot -a 100 -b 100 -x -3.3078 -y -6 -z 5.2 -p 2 -c \#0d867a -o ../public/5dd14670fcb9aa5868b9ab10-5dd14670eacc3a64be2cbb94.png
