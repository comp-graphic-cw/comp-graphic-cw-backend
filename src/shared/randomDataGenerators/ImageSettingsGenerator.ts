import _reduce from 'lodash/reduce';
import { ImagePresetModel } from '../../database/entities/ImagePresets/ImagePreset.model';
import config from '../../config/server';
import { ImageConfiguration } from '../../typings';
import { hexToRgb, hsvToHex, RGB, rgb2hsv } from '../utils';

const valueBetweenGenerator = (min: number, max: number, round = 4): (() => number) => () =>
    +Number((min + Math.random() * (max - min)).toFixed(round));

const generateColor = (): ((arg0: { color: string }) => string) => ({ color: rgbHex }: { color: string }) => {
    let rgb: RGB;

    if (rgbHex) {
        rgb = hexToRgb(rgbHex);
    } else {
        const genColor = valueBetweenGenerator(0, 255, 0);

        rgb = {
            r: genColor(),
            g: genColor(),
            b: genColor(),
        };
    }

    const hsv = rgb2hsv(rgb);

    return hsvToHex(hsv);
};

const generationOptions = {
    presetId: (v: { id: string }): string => v.id,
    centerX: valueBetweenGenerator(config.fractalsOptions.minCenterX, config.fractalsOptions.maxCenterX),
    centerY: valueBetweenGenerator(config.fractalsOptions.minCenterY, config.fractalsOptions.maxCenterY),
    maxZ: valueBetweenGenerator(config.fractalsOptions.minZ, config.fractalsOptions.maxZ, 0),
    color: generateColor(),
    power: valueBetweenGenerator(config.fractalsOptions.minPower, config.fractalsOptions.maxPower, 2),
};

const fields = [...Object.keys(generationOptions), 'width', 'height', 'type'];

const iterateOver = <T>(initialData: object): T =>
    _reduce(
        fields,
        (mem, key) => {
            if (!initialData[key] || key === 'color') {
                mem[key] = generationOptions[key](initialData);
            } else {
                mem[key] = initialData[key];
            }

            return mem;
        },
        {},
    ) as any;

export const generateImageSettings = (initialData: ImagePresetModel, length: number): ImageConfiguration[] =>
    Array.from({ length }, () => iterateOver<ImageConfiguration>(initialData));
