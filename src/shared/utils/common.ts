import { Environment } from '../../constants';

export const isTest = (env: Environment): boolean => env === Environment.TEST;

export const isDev = (env: Environment): boolean => env !== Environment.PRODUCTION;
