export interface HSV {
    h: number;
    s: number;
    v: number;
}

export interface RGB {
    r: number;
    g: number;
    b: number;
}

export const rgb2hsv = (rgb: RGB): HSV => {
    const rabs = rgb.r / 255;
    const gabs = rgb.g / 255;
    const babs = rgb.b / 255;
    const v = Math.max(rabs, gabs, babs);
    const diff = v - Math.min(rabs, gabs, babs);
    const diffc = (c: number): number => (v - c) / 6 / diff + 1 / 2;
    const percentRoundFn = (num: number): number => Math.round(num * 100) / 100;
    let h;
    let s;

    if (diff === 0) {
        h = 0;
        s = 0;
    } else {
        s = diff / v;
        const rr = diffc(rabs);
        const gg = diffc(gabs);
        const bb = diffc(babs);

        if (rabs === v) {
            h = bb - gg;
        } else if (gabs === v) {
            h = 1 / 3 + rr - bb;
        } else if (babs === v) {
            h = 2 / 3 + gg - rr;
        }

        if (h < 0) {
            h += 1;
        } else if (h > 1) {
            h -= 1;
        }
    }

    return {
        h: Math.round(h * 360),
        s: percentRoundFn(s * 100),
        v: percentRoundFn(v * 100),
    };
};

export const toHex = (num: number): string => {
    let hex = Number(~~num).toString(16);

    if (hex.length < 2) {
        hex = `0${hex}`;
    }

    return hex;
};

export const hsvToHex = (hsv: HSV): string => `${toHex(hsv.h)}${toHex(hsv.s)}${toHex(hsv.v)}`;

export const hexToRgb = (hex: string): RGB => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    if (!result) {
        throw new Error(`Cannot convert non hex string ${hex} to rgb`);
    }

    return {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
    };
};
