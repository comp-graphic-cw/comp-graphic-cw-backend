import _reduce from 'lodash/reduce';

type ConfigContainer = string | number | object | ConfigContainer[];

export const extendAnyWithEnvVars = <T extends ConfigContainer>(source: ConfigContainer): T => {
    if (Array.isArray(source)) {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return extendArrayWithEnvVars(source) as T;
    }

    if (typeof source === 'object') {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return extendObjectWithEnvVars(source) as T;
    }

    if (typeof source === 'string') {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return replaceStringWithEnvVar(source) as T;
    }

    return source as T;
};

export const extendArrayWithEnvVars = (arr: ConfigContainer[]): ConfigContainer[] =>
    _reduce(
        arr,
        (mem: [], value: ConfigContainer) => {
            mem.push(extendAnyWithEnvVars(value));
            return mem;
        },
        [],
    );

export const replaceStringWithEnvVar = (source: string): string | undefined => {
    if (source.startsWith('$')) {
        return process.env[source.slice(1)];
    }

    return source;
};

export const extendObjectWithEnvVars = (source: object): object =>
    _reduce(
        source,
        (mem: {}, value: ConfigContainer, key: string) => {
            mem[key] = extendAnyWithEnvVars(value);
            return mem;
        },
        {},
    );
