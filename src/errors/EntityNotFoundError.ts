import { ApiError } from './ApiError';

export class EntityNotFoundError extends ApiError {
    constructor(entityName: string) {
        super({
            message: `Requested ${entityName} was not found`,
            status: 404,
        });
        Object.setPrototypeOf(this, ApiError.prototype);
    }
}
