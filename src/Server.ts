import * as http from 'http';
import Koa from 'koa';
import { useContainer, useKoaServer } from 'routing-controllers';
import { Container } from 'typedi';
import koaBodyparser from 'koa-bodyparser';
import compress from 'koa-compress';
import koaLogger from 'koa-logger';
import cors from '@koa/cors';
import { Environment, ServerEvents } from './constants';
import { ImagesPresetsController } from './controllers/imagePresets/ImagesPresets.controller';
import { RootController } from './controllers/Root.controller';
import { ErrorGuardMiddleware } from './middlewares/error-guard.middleware';
import { ResponseHelpersMiddleware } from './middlewares/response-wrappers.middleware';
import { isDev, isTest } from './shared/utils/common';
import { ServerDefaultContext } from './typings';
import { getLogger } from './shared/Logger';

const httpLogger = getLogger('http');

export class Server extends Koa<ServerDefaultContext> {
    private server: http.Server;

    private readonly env: Environment;

    constructor({ env }: { env: Environment }) {
        super();
        this.env = env;
        this.prepare();
    }

    // @ts-ignore
    public listen(port: number): http.Server {
        this.server = super.listen(port);
        this.emit(ServerEvents.LISTEN, { port });
        return this.server;
    }

    stop() {
        this.server.close();
        this.emit(ServerEvents.CLOSE);
        return this.server;
    }

    private prepareContext(): void {
        this.context.isDev = isDev(this.env);
        this.context.isTest = isTest(this.env);
    }

    private prepareAPI(): void {
        useContainer(Container);
        useKoaServer(this, {
            defaultErrorHandler: false,
            routePrefix: '/api/v1',
            middlewares: [ResponseHelpersMiddleware, ErrorGuardMiddleware],
            controllers: [RootController, ImagesPresetsController],
        });
    }

    private prepareMiddlewares(): void {
        this.use(cors())
            .use(koaBodyparser({ enableTypes: ['json'], jsonLimit: '1mb' }))
            .use(
                koaLogger((str) => {
                    httpLogger.info(str);
                }),
            )
            .use(compress());
    }

    private prepare(): void {
        this.prepareMiddlewares();
        this.prepareAPI();
        this.prepareContext();
    }
}
