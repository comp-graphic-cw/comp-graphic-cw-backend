import { classToPlain } from 'class-transformer';
import { ReadStream } from 'fs';
import { Body, Ctx, Delete, Get, Header, JsonController, Param, Post, Redirect } from 'routing-controllers';
import { Inject } from 'typedi';
import { PRE_GENERATED_IMAGES_COUNT } from '../../constants';
import { EntityNotFoundError } from '../../errors/EntityNotFoundError';
import { ImagesService } from '../../services/Images.service';
import { ImagesStatsService } from '../../services/ImagesStats.service';
import { ServerDefaultContext } from '../../typings';
import { GenerateImageWorker } from '../../worker/GenerateImage.worker';
import { CreateImagePresetApiDto } from './CreateImagePresetApi.dto';

@JsonController('/image-presets')
export class ImagesPresetsController {
    @Inject()
    private imagePresetsService: ImagesService;

    @Inject()
    private worker: GenerateImageWorker;

    @Inject()
    private imageStatsService: ImagesStatsService;

    @Post('/')
    public async createImagePreset(@Body() data: CreateImagePresetApiDto): Promise<object> {
        return (await this.imagePresetsService.createAsset(data)).getPlain();
    }

    @Delete('/jobs')
    public async deleteImageGenerationJobs(): Promise<string> {
        await this.imagePresetsService.jobsService.deleteAllJobsGenerateImage();
        return 'deleted';
    }

    @Get('/image-by-config/:id')
    @Header('Content-type', 'image/png')
    public async getImageFile(@Param('id') id: string, @Ctx() ctx: ServerDefaultContext): Promise<ReadStream> {
        const imageStat = await this.imageStatsService.getStatById(id);

        if (!this.imageStatsService.imageExists(id)) {
            await this.worker.generateImageImmediately(id);
            this.imagePresetsService.generateNewConfigForImage(imageStat.preset.presetId, PRE_GENERATED_IMAGES_COUNT);
        }

        // todo: move to queue
        setTimeout(() => this.imageStatsService.markStatAsUsed(id), 5000);
        this.imagePresetsService.generateNewConfigForImage(imageStat.preset.presetId, 1);
        ctx.doNotWrapResponse = true;
        ctx.set('image-config', JSON.stringify(classToPlain(imageStat)));
        return this.imageStatsService.getImageReadStream(id);
    }

    @Get('/random-image/:id')
    @Redirect('/')
    public async getRandomImageFile(@Param('id') id: string, @Ctx() ctx: ServerDefaultContext): Promise<string> {
        ctx.doNotWrapResponse = true;
        const randomInstance = await this.imageStatsService.getRandomInstanceByPresetId(id);

        if (!randomInstance) {
            throw new EntityNotFoundError('image');
        }

        return `/api/v1/image-presets/image-by-config/${randomInstance.id}`;
    }

    @Get('/')
    public async getImages(): Promise<object[]> {
        const imagePresets = await this.imagePresetsService.getAll();

        return [...imagePresets];
    }

    @Get('/:id')
    public async getImage(@Param('id') id: string): Promise<object> {
        const imagePreset = classToPlain(await this.imagePresetsService.getById(id));

        if (!imagePreset) {
            throw new EntityNotFoundError('imagePreset');
        }

        const imageStat = await this.imageStatsService.getStatByPresetId(id);

        const randomInstance = classToPlain(await this.imageStatsService.getRandomInstanceByPresetId(id));

        return {
            ...imagePreset,
            randomInstance,
            executionTime: imageStat.executionTime,
        };
    }
}
