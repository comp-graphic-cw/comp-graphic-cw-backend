import { IsIn, IsOptional, Min, IsInt, Max } from 'class-validator';
import { FractalTypes } from '../../constants';

export class CreateImagePresetApiDto {
    @IsIn([FractalTypes.JULIA, FractalTypes.MANDELBROT])
    public readonly type: FractalTypes;

    @Min(1)
    @IsInt()
    public readonly width: number;

    @Min(1)
    @IsInt()
    public readonly height: number;

    @IsOptional()
    public readonly centerX: number | null;

    @IsOptional()
    public readonly centerY: number | null;

    @IsOptional()
    @IsInt()
    public readonly maxZ: number | null;

    @IsOptional()
    public readonly color: string | null;

    @IsOptional()
    public readonly power: number | null;

    @IsOptional()
    @Max(500)
    public readonly iterations: number | null;
}
