import * as http from 'http';
import 'reflect-metadata';
import { MongoClient } from 'mongodb';
import { Redis } from 'ioredis';
import { Container } from 'typedi';
import config from '../config/server';
import { Instance } from '../constants';
import { Server } from '../Server';
import { init as initLogger, getLogger } from '../shared/Logger';
import { connect as mongoConnect, close as mongoClose } from '../database/mongo';
import { connect as redisConnect, close as redisClose } from '../database/redis';

initLogger(config);

/**
 * init DI container
 */
Container.set(Instance.CONFIG, config);

const logger = getLogger('bin');

const initializeDbConnections = async (): Promise<void> => {
    const [mongo, redis] = await Promise.all<MongoClient | Redis>([
        mongoConnect(config.mongo.uri, config.mongo.options),
        redisConnect(config.redis),
    ]);

    // Container.set(Instance.SEQUELIZE, await sequelizeConnect(config.db));
    Container.set(Instance.MONGO, mongo);
    Container.set(Instance.REDIS, redis);
};

const closeDBConnections = async (): Promise<void> => {
    await mongoClose(Container.get<MongoClient>(Instance.MONGO));
    await redisClose(Container.get<Redis>(Instance.REDIS));
};

const handleServerError = (error: Error) => {
    logger.error('Server error: %j', error.toString());
};
const handleProcessError = (reason: any) => {
    logger.error('Unhandled error: %s', reason);
};

const upServer = (app: Server): http.Server => {
    const server = app.listen(config.port);

    server.on('error', handleServerError);

    return server;
};

const gracefullyShutdown = (app: Server): NodeJS.SignalsListener => async (signal: NodeJS.Signals): Promise<void> => {
    logger.info('Receive signal [%s]. Stop all', signal);
    await app.stop();
    await closeDBConnections();
    logger.info('Server was stopped, caused by [%s]', signal);
};

const bindProcessEventListeners = (app: Server) => {
    process.on('unhandledRejection', handleProcessError);
    process.on('uncaughtException', handleProcessError);
    ['SIGUSR2', 'SIGTERM', 'SIGINT'].forEach((signal: NodeJS.Signals) => process.on(signal, gracefullyShutdown(app)));
};

const start = async (): Promise<void> => {
    await initializeDbConnections();
    const app: Server = new Server(config);

    upServer(app);
    bindProcessEventListeners(app);
};

start();
