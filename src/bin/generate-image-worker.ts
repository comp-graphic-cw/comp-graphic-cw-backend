import 'reflect-metadata';
import { MongoClient } from 'mongodb';
import { Redis } from 'ioredis';
import { Container } from 'typedi';
import config from '../config/server';
import { Instance } from '../constants';
import { JobsProcessorService } from '../services/JobsProcessor.service';
import { GenerateImageWorker } from '../worker/GenerateImage.worker';
import { init as initLogger, getLogger } from '../shared/Logger';
import { connect as mongoConnect, close as mongoClose } from '../database/mongo';

initLogger(config);

/**
 * init DI container
 */
Container.set(Instance.CONFIG, config);
Container.set(Instance.REDIS, null);

const logger = getLogger('bin');

const initializeDbConnections = async (): Promise<void> => {
    const [mongo] = await Promise.all<MongoClient | Redis>([mongoConnect(config.mongo.uri, config.mongo.options)]);

    Container.set(Instance.MONGO, mongo);
};

const closeDBConnections = async (): Promise<void> => {
    await mongoClose(Container.get<MongoClient>(Instance.MONGO));
    await Container.get<JobsProcessorService>(JobsProcessorService).close();
};

const handleProcessError = (reason: any) => {
    logger.error('Unhandled error: %s', reason);
};

const gracefullyShutdown = (worker: GenerateImageWorker): NodeJS.SignalsListener => async (
    signal: NodeJS.Signals,
): Promise<void> => {
    logger.info('Receive signal [%s]. Stop all', signal);
    await worker.stop();
    await closeDBConnections();
    logger.info('GenerateImageProcessor was stopped, caused by [%s]', signal);
};

const runWorker = async (worker: GenerateImageWorker): Promise<void> => {
    await worker.startWorking();
};

const bindProcessEventListeners = (worker: GenerateImageWorker): void => {
    process.on('unhandledRejection', handleProcessError);
    process.on('uncaughtException', handleProcessError);
    ['SIGUSR2', 'SIGTERM', 'SIGINT'].forEach((signal: NodeJS.Signals) =>
        process.on(signal, gracefullyShutdown(worker)),
    );
};

const start = async (): Promise<void> => {
    await initializeDbConnections();
    const worker: GenerateImageWorker = Container.get<GenerateImageWorker>(GenerateImageWorker);

    await runWorker(worker);
    bindProcessEventListeners(worker);
};

start();
