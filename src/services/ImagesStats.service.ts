import { createReadStream, existsSync, ReadStream, unlinkSync } from 'fs';
import { MongoClient } from 'mongodb';
import * as path from 'path';
import { Inject, Service } from 'typedi';
import { ImageStatModel } from '../database/entities/ImageStat/ImageStat.model';
import ImageStatRepository from '../database/entities/ImageStat/ImageStat.repository';
import { getLogger } from '../shared/Logger';
import { ImageConfiguration } from '../typings';

export interface CreateImageStatDto {
    preset: ImageConfiguration;
    id: string;
    executionTime: number;
}

const logger = getLogger('ImagesStatsService');

@Service()
export class ImagesStatsService {
    @Inject()
    readonly mongo: MongoClient;

    @Inject()
    readonly imageStatRepository: ImageStatRepository;

    public async createStat(data: CreateImageStatDto): Promise<string> {
        const { insertedId } = await this.imageStatRepository.collection.insertOne({ _id: data.id, ...data });

        return insertedId.toString();
    }

    public async getStatByPresetId(presetId: string): Promise<{ executionTime: number | null }> {
        const [result] = await this.imageStatRepository.collection
            .aggregate([
                {
                    $match: { 'preset.presetId': presetId },
                },
                {
                    $group: {
                        _id: '$preset.presetId',
                        executionTime: { $avg: '$executionTime' },
                    },
                },
            ])
            .toArray();

        if (result) {
            return {
                executionTime: result.executionTime,
            };
        }

        return {
            executionTime: null,
        };
    }

    public async getStatById(presetId: string): Promise<ImageStatModel | null> {
        const doc = await this.imageStatRepository.collection.findOne({ _id: presetId });

        if (!doc) return null;

        return ImageStatModel.fromMongoDoc(doc);
    }

    public async markStatAsUsed(presetId: string): Promise<void> {
        await this.imageStatRepository.collection.updateOne({ _id: presetId }, { $set: { used: true } });
        try {
            unlinkSync(this.getPathToImage(presetId));
        } catch {
            logger.error('unable to unlink file %s', this.getPathToImage(presetId));
        }
    }

    public async getRandomInstanceByPresetId(presetId: string): Promise<ImageStatModel> {
        const [doc] = await this.imageStatRepository.collection
            .aggregate([
                {
                    $match: {
                        $expr: {
                            $and: [{ $eq: ['$preset.presetId', presetId] }],
                        },
                    },
                },
                { $sample: { size: 1 } },
            ])
            .toArray();

        if (!doc) return null;

        return ImageStatModel.fromMongoDoc(doc);
    }

    public getImageReadStream(imageConfigId: string): ReadStream {
        return createReadStream(this.getPathToImage(imageConfigId));
    }

    private getPathToImage(imageConfigId: string): string {
        return path.join(__dirname, '../../public', `${imageConfigId}.png`);
    }

    public imageExists(id: string): boolean {
        return existsSync(this.getPathToImage(id));
    }
}
