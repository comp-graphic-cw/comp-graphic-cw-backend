import Bull, { Job } from 'bull';
import { Service } from 'typedi';
import { JobsQueue } from '../constants';
import { getLogger } from '../shared/Logger';
import { ImageConfiguration } from '../typings';

const logger = getLogger('JobsProcessorService');

export interface JobsProcessor {
    process: (job: object) => Promise<void>;
}

@Service()
export class JobsProcessorService {
    private readonly generateImageQueue: Bull.Queue<ImageConfiguration>;

    constructor() {
        this.generateImageQueue = new Bull(JobsQueue.GENERATE_IMAGE, { redis: 'redis://127.0.0.1:6379' });
    }

    private logJobCompleted(job: Job): void {
        logger.info('complete job from queue [%s] with jobId [%s] and payload %j', job.queue.name, job.name, job.data);
    }

    private logJobReceived(queue: JobsQueue, jobId: string, data: {}): void {
        logger.info('receive job from queue [%s] with jobId [%s] and payload %j', queue, jobId, data);
    }

    private logTryToSubscribeToQueue(queue: JobsQueue): void {
        logger.info('trying to subscribe to jobs queue %s', queue);
    }

    private logTryToUnsubscribeFromQueue(queue: JobsQueue): void {
        logger.info('trying to unsubscribe from queue %s', queue);
    }

    private logUnsubscribedToQueue(queue: JobsQueue): void {
        logger.info('unsubscribed from queue %s', queue);
    }

    private logUnsubscriptionToQueueFail(queue: JobsQueue, reason: Error): void {
        logger.error('unable to unsubscribe from queue %s because: %s', queue, reason.message);
    }

    private logSubscribedToQueue(queue: JobsQueue): void {
        logger.info('subscribed to jobs queue %s', queue);
    }

    private logSubscriptionsToQueueFail(queue: JobsQueue, reason: Error): void {
        logger.error('unable to subscribe to jobs queue %s because: %s', queue, reason.message);
    }

    private logUnableToProcessJob(job: Job, reason: Error): void {
        logger.error('catch an error while process job [%s] from queue[%s]: %s', job.id, job.queue.name, reason);
    }

    async subscribeToGenerateImageJobs(handler: JobsProcessor): Promise<void> {
        await this.generateImageQueue.resume(false);

        try {
            this.logTryToSubscribeToQueue(JobsQueue.GENERATE_IMAGE);
            this.generateImageQueue.process('*', 10, async (job: Job<ImageConfiguration>) => {
                this.logJobReceived(JobsQueue.GENERATE_IMAGE, job.name, job.data);
                try {
                    await handler.process(job.data);
                } catch (e) {
                    this.logUnableToProcessJob(job, e);
                    throw e;
                }
                this.logJobCompleted(job);
            });
            this.logSubscribedToQueue(JobsQueue.GENERATE_IMAGE);
        } catch (e) {
            this.logSubscriptionsToQueueFail(JobsQueue.GENERATE_IMAGE, e);
            throw e;
        }
    }

    async unsubscribeToGenerateImageJobs(): Promise<void> {
        try {
            this.logTryToUnsubscribeFromQueue(JobsQueue.GENERATE_IMAGE);
            await this.generateImageQueue.pause(true);
            this.logUnsubscribedToQueue(JobsQueue.GENERATE_IMAGE);
        } catch (e) {
            this.logUnsubscriptionToQueueFail(JobsQueue.GENERATE_IMAGE, e);
            throw e;
        }
    }

    async close(): Promise<void> {
        await this.generateImageQueue.close(false);
    }
}
