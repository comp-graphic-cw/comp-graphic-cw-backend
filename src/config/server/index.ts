import * as dotenv from 'dotenv';
import * as YAML from 'js-yaml';
import * as fse from 'fs-extra';
import * as path from 'path';
import { Environment } from '../../constants';
import { extendAnyWithEnvVars } from '../../shared/utils/objects';
import { configSchema, ServerConfig } from './server.schema';

export { ServerConfig } from './server.schema';

dotenv.config();

const getEnv = (): Environment => {
    const env = process.env.NODE_ENV;

    if (!env) {
        return Environment.DEV;
    }

    switch (env) {
        case Environment.PRODUCTION:
            return Environment.PRODUCTION;
        case Environment.DEV:
            return Environment.DEV;
        case Environment.TEST:
            return Environment.TEST;
        default:
            throw new Error(`Process started with unknown environment: ${env}`);
    }
};

const getYamlConfigFilePath = (env: string): string => path.join(__dirname, '../../../config', env, 'server.yaml');

const loadConfigFromYaml = (env: Environment): string => {
    const configFilePath = getYamlConfigFilePath(env);

    try {
        return YAML.safeLoad(fse.readFileSync(configFilePath).toString());
    } catch {
        throw new Error(`Missing file ${configFilePath}`);
    }
};

export const loadConfig = (): ServerConfig => {
    const rawConfig = loadConfigFromYaml(getEnv());
    const patchedConfig = extendAnyWithEnvVars<ServerConfig>(rawConfig);

    const { value: validatedConfig, error } = configSchema.validate(patchedConfig, {
        presence: 'required',
        stripUnknown: true,
        abortEarly: true,
    });

    if (error) {
        throw new Error(`Error while validate config ${JSON.stringify(error.details, null, 2)}`);
    }

    return validatedConfig;
};

export default loadConfig();
