import Redis from 'ioredis';
import { getLogger } from '../shared/Logger';

const logger = getLogger('redis');

export const connect = async (config: Redis.RedisOptions): Promise<Redis.Redis> => {
    logger.info('establish connection....');
    try {
        const client = new Redis(config);

        await client.ping();
        logger.info('connected');

        return client;
    } catch (e) {
        logger.error('unable to connect: %s', e.message);
        throw e;
    }
};

export const close = (redisClient: Redis.Redis): void => {
    logger.info('closing connection....');
    redisClient.disconnect();
    logger.info('connection closed');
};
