import { Collection, MongoClient } from 'mongodb';
import { Inject, Service } from 'typedi';
import { Instance } from '../../../constants';

@Service()
export default class ImageStatRepository {
    public readonly collection: Collection;

    public static readonly collectionName: string = 'imageStats';

    constructor(@Inject(Instance.MONGO) private mongo: MongoClient) {
        this.collection = this.mongo.db().collection(ImageStatRepository.collectionName);
    }
}
