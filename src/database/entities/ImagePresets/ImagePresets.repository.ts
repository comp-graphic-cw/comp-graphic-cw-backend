import { Collection, MongoClient } from 'mongodb';
import { Inject, Service } from 'typedi';
import { Instance } from '../../../constants';

@Service()
export default class ImagePresetsRepository {
    public readonly collection: Collection;

    public static readonly collectionName: string = 'imagePresets';

    constructor(@Inject(Instance.MONGO) private mongo: MongoClient) {
        this.collection = this.mongo.db().collection(ImagePresetsRepository.collectionName);
    }
}
