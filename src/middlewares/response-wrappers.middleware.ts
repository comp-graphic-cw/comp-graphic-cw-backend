import { Middleware, KoaMiddlewareInterface } from 'routing-controllers';
import { ServerDefaultContext } from '../typings';
import { wrapSuccess } from '../shared/responseWrappers';

const clearEmptyBodyFields = (ctx: ServerDefaultContext) => {
    if (ctx.body) {
        Object.keys(ctx.body).forEach((key) => {
            if (ctx.body[key] === null || ctx.body[key] === undefined) {
                delete ctx.body[key];
            }
        });
    }
};

@Middleware({ type: 'before' })
export class ResponseHelpersMiddleware implements KoaMiddlewareInterface {
    async use(ctx: ServerDefaultContext, next: () => any): Promise<void> {
        await next();
        if (!ctx.doNotWrapResponse && ctx.body && (!ctx.status || ctx.status < 300)) {
            wrapSuccess(ctx, { data: ctx.body });
        }

        clearEmptyBodyFields(ctx);
    }
}
