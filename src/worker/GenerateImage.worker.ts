import { Inject, Service } from 'typedi';
import { spawn } from 'child_process';
import { ObjectID } from 'mongodb';
import { PYTHON_EXECUTABLE_FILE, PYTHON_GENERATOR_SCRIPT_PATH } from '../constants';
import { CreateImageStatDto, ImagesStatsService } from '../services/ImagesStats.service';
import { JobsProcessor, JobsProcessorService } from '../services/JobsProcessor.service';
import { getLogger } from '../shared/Logger';
import { ImageConfiguration } from '../typings';

const logger = getLogger('GenerateImageWorker');

@Service()
export class GenerateImageWorker implements JobsProcessor {
    @Inject()
    private readonly jobsService: JobsProcessorService;

    @Inject()
    private readonly imagesStatService: ImagesStatsService;

    async generateImageImmediately(id: string): Promise<void> {
        const stat = await this.imagesStatService.getStatById(id);

        if (!stat) throw new Error(`No such image settings for id: ${id}`);

        const { preset } = stat;

        await this.processStat(preset, stat.id);
    }

    async startWorking(): Promise<void> {
        await this.jobsService.subscribeToGenerateImageJobs(this);
    }

    public getGeneratorProgramArgs(job: ImageConfiguration, generatedImageInfoId: string): string[] {
        return [
            '-f',
            job.type,
            '-a',
            job.width,
            '-b',
            job.height,
            '-x',
            job.centerX,
            '-y',
            job.centerY,
            '-z',
            job.maxZ,
            '-p',
            job.power,
            '-c',
            `${job.color}`,
            '-o',
            `../public/${generatedImageInfoId}.png`,
        ].map((v: any) => String(v));
    }

    private async generateImageBasedOnSettings(job: ImageConfiguration, imageStatId: string): Promise<void> {
        return new Promise((resolve, reject) => {
            const args = [PYTHON_EXECUTABLE_FILE, ...this.getGeneratorProgramArgs(job, imageStatId)];

            logger.info('spawn: env/bin/python %s', args.join(' '));

            const child = spawn('env/bin/python', args, {
                cwd: PYTHON_GENERATOR_SCRIPT_PATH,
            });

            child.stdout.on('data', (data) => {
                logger.info(`[python script] data: ${data}`);
            });
            child.stderr.on('data', (data) => {
                logger.error(`[python script] data: ${data}`);
            });
            child.on('error', (e) => {
                logger.error('[python script] error: %s', e.toString());
                reject(e);
            });
            child.on('close', (code) => {
                logger.info(`[python script] ended with code: ${code}`);
                if (code !== 0) {
                    return reject(new Error('Child process ended with non 0 code'));
                }

                return resolve();
            });
        });
    }

    private createImageStatRecord({ preset, id, executionTime }: CreateImageStatDto): Promise<string> {
        return this.imagesStatService.createStat({ id, preset, executionTime });
    }

    public async process(job: ImageConfiguration): Promise<void> {
        const imageStatId = new ObjectID().toString();
        const startTime = Date.now();

        await this.processStat(job, imageStatId);
        await this.createImageStatRecord({ preset: job, id: imageStatId, executionTime: Date.now() - startTime });
    }

    public async processStat(job: ImageConfiguration, imageStatId: string): Promise<void> {
        await this.generateImageBasedOnSettings(job, imageStatId);
    }

    public async stop(): Promise<void> {
        await this.jobsService.unsubscribeToGenerateImageJobs();
    }
}
