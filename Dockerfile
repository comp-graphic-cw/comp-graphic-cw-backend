FROM node:10-alpine as base
LABEL version="1.0"

ENV NODE_ENV development

WORKDIR /app
ADD package.json .
RUN npm i
ADD . /app
RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "exec:server"]

